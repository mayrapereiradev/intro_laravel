
## Marketplace Laravel

Sistema web Marketplace com pagamento via PagSeguro

## Configurações necessárias (exemplo)

- Banco de dados

##


- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=marketplace
- DB_USERNAME=root
- DB_PASSWORD=root

## 

- Envio de Emails

## 
- MAIL_DRIVER=smtp
- MAIL_HOST=smtp.gmail.com
- MAIL_PORT=587
- MAIL_USERNAME=seuEmail@email.com
- MAIL_PASSWORD=password
- MAIL_ENCRYPTION=tls
- MAIL_FROM_ADDRESS=seuEmail@email.com


## 
- Chaves do PagSeguro

## 

- PAGSEGURO_ENV=sandbox
- PAGSEGURO_EMAIL=seuEmail@email.com
- PAGSEGURO_TOKEN_SANDBOX=(seu toekn aqui)
- PAGSEGURO_CHARSET=UTF-8


