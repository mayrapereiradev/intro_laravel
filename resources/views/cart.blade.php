@extends('layouts.front')

@section('content')

    <div class="row">
        <div class="col-12">
            <h2>Carrinho de Compra</h2>
            <hr>
        </div>
        <div class="col-12">
            @if ($cart)
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Produto</th>
                        <th>Preço</th>
                        <th>Qantidade</th>
                        <th>Subtotal</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $total = 0; @endphp
                    @foreach($cart as $product)
                        <tr>
                            <td>{{$product['name']}}</td>
                            <td>R$ {{number_format($product['price'] , 2, ',', '.' )}}</td>
                            <td>{{$product['amount']}}</td>
                            <td>R$ {{number_format($product['price'] * $product['amount'], 2, ',', '.')}}</td>
                            @php
                                $subtotal = $product['price'] * $product['amount'];
                                $total += $subtotal;
                            @endphp
                            <td>
                                <a href="{{route('cart.remove', ['slug' => $product['slug']])}}" class="btn btn-sm btn-danger">Remover</a>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3">Total</td>
                        <td colspan="2">{{number_format($total, 2, ',', '.')}}</td>
                    </tr>
                    </tbody>
                </table>
                <hr>
                <div class="col-md-12">
                    <a href="{{route('checkout.index')}}" class="btn btn-lg btn-success pull-right">Concluir Compra</a>
                    <a href="{{route('cart.cancel')}}" class="btn btn-lg btn-danger pull-left">Cancelar Compra</a>
                </div>
            @else
                <div class="alert alert-warning">Carrinho vazio!</div>
            @endif
        </div>
    </div>
@endsection
