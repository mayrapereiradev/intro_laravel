@extends('layouts.app')

@section('content')
    <h1 class="mt-2">Criar categoria</h1>

    <form action="{{ route('admin.categories.update', ['category' => $category]) }}" method="post">
        @csrf
        @method("PUT")
        <div class="form-group">
            <label>Nome</label>
            <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{$category->name}}">
            @error('name')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>

        <div class="form-group">
            <label>Descrição</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control @error('description') is-invalid @enderror">{{$category->description}}</textarea>
            @error('description')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>


        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-success">Criar Categoria</button>
        </div>

    </form>
@endsection
