
@extends('layouts.app')

@section('content')
    <h1 class="mt-2">Lista de Categorias</h1>
    <a href="{{ route('admin.categories.create')  }}" class="btn btn-sm btn-success mt-1 mb-1">Nova Categoria</a>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td>{{ $category->name }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('admin.categories.edit', ['category' =>$category->id])  }}" class="btn btn-sm btn-primary mr-2">EDITAR</a>
                        <form action="{{ route('admin.categories.destroy', ['category' =>$category->id]) }}" method="post">
                            @csrf
                            @method("DELETE")
                            <button class="btn btn-sm btn-danger" type="submit">REMOVER</button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$categories->links()}}

@endsection
