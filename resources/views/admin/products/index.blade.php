@extends('layouts.app')

@section('content')
    <h1 class="mt-2">Lista de Produtos</h1>
    <a href="{{ route('admin.products.create')  }}" class="btn btn-sm btn-success mt-1 mb-1">Novo Produto</a>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Preço</th>
            <th>Loja</th>
            <th>Ações</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td>R$ {{ number_format($product->price, 2, ',', '.') }}</td>
                <td>{{ $product->store->name }}</td>
                <td>
                    <div class="btn-group">
                        <a href="{{ route('admin.products.edit', ['product' =>$product->id])  }}" class="btn btn-sm btn-primary mr-2">EDITAR</a>
                        <form action="{{ route('admin.products.destroy', ['product' =>$product->id]) }}" method="post">
                            @csrf
                            @method("DELETE")
                            <button class="btn btn-sm btn-danger" type="submit">REMOVER</button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{$products->links()}}

@endsection
