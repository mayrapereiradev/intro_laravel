<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\Store;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store = auth()->user()->store;

        $products = Product::where('store_id', $store->id)->paginate(10);

        return view('admin.products.index', [
            'products' => $products,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all(['id', 'name']);
        return view('admin.products.create', [
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        $data = $request->all();

        $categories = $request->get('categories', null);

        $store = auth()->user()->store;
        $product = $store->products()->create($data);
        $product->categories()->sync($categories);

        if($request->hasFile('photos')){
            $images = $this->imageUpload($request->file('photos'), 'image');
            //inserir referencia na base
            $product->photos()->createMany($images);
        }

        flash('Produto criado com sucesso!')->success();
        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all(['id', 'name']);

        return view('admin.products.edit', [
            'product' => $product,
            'categories' => $categories,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $data = $request->all();

        $categories = $request->get('categories', null);

        $product = Product::find($id);
        $product->update($data);
        
        if (!isset($data['categories'])) {
            $product->categories()->detach();
        } else {
            $product->categories()->sync($data['categories']);

        }

        if($request->hasFile('photos')){
            $images = $this->imageUpload($request->file('photos'), 'image');
            //inserir referencia na base
            $product->photos()->createMany($images);
        }

        $categories = Category::all(['id', 'name']);

        flash('Produto alterado com sucesso!')->success();
        return redirect()->route('admin.products.edit', [
            'product' => $product,
            'categories' => $categories,
        ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->categories()->detach($id);
        $product->delete();
        flash('Produto removido com sucesso!')->success();
        return redirect()->route('admin.products.index');

    }
}
