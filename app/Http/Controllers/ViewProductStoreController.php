<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;

class ViewProductStoreController extends Controller
{
    private $store;

    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    public function index($slug)
    {
        $store = $this->store->whereSlug($slug)->first();

        return view('store', [
            'store' => $store,
        ]);
    }

}
